var isEqual = (a, b, eps) => {
    return Math.abs(a - b) < eps
}

var a = 0.1 * 3
var b = 0.3

var x = 10000.1 * 3
var y = 30000.3

var relTol = 0.01 * 100
var tol = relTol * Math.max(Math.abs(a), Math.abs(b));

console.log(isEqual(a,b, tol))
