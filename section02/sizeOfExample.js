// npm i sizeof

var sizeof = require('sizeof');
var anyObject = {
    'key': {
        name: 'abc',
        age: 123,
        active: true
    }
}
console.log(sizeof.sizeof(anyObject));
console.log(sizeof.sizeof(anyObject, true));