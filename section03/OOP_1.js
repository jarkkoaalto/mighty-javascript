var user1 = {
    name:"John Doe",
    items:0,
    increment(){
        return ++user1.items
    }
}
console.log(user1.increment())

// DOT notation

var user2 = {}
user2.name="Jane Doe"
user2.items = 2
user2.increment = function increment(){
    return ++user2.items
}
console.log(user2.increment())

var user3 = Object.create(null)
console.log(user3)

var user3 = Object.create(user1)

user3.item = 3
user3.increment = function increment(){
    return ++user3.item
}
console.log(user3.increment())