function users(name, items){
    var newUser = {}
    newUser.name = name
    newUser.items = items
    newUser.increment = function increment() {
        return ++newUser.items
    }

    return newUser
}

var user1 = users("John Doe",2)
var user2 = users("Jane Doe",3)

console.log(user1.increment())
console.log(user2.increment())